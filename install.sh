#!/bin/bash

set -e

ask_parameters() {

  export PSONO_PROTOCOL="https://"
  export PSONO_VERSION=EE
  export PSONO_EXTERNAL_PORT=80
  export PSONO_EXTERNAL_PORT_SECURE=443
  export PSONO_POSTGRES_PORT=5432
  export PSONO_POSTGRES_PASSWORD=PSONO_IS_THE_BEST_OPEN_SOURCE_PASSWORD_MANAGER
  export PSONO_USERDOMAIN=localhost
  export PSONO_WEBDOMAIN=localhost
  export PSONO_POSTGRES_USER=postgres
  export PSONO_POSTGRES_DB=postgres
  export PSONO_POSTGRES_HOST=postgres
  export PSONO_INSTALL_ACME=0

  if [ -f "$HOME/.psonoenv" ]; then
    set -o allexport
    source ~/.psonoenv
    set +o allexport
  fi

  echo "What version do you want to install? (Usually EE. Potential other choices are CE or DEV)"
  read -p "PSONO_VERSION [default: $PSONO_VERSION]: " PSONO_VERSION_NEW
  if [ "$PSONO_VERSION_NEW" != "" ]; then
    export PSONO_VERSION=$PSONO_VERSION_NEW
  fi

  if [[ ! $PSONO_VERSION =~ ^(DEV|EE|CE)$ ]]; then
    echo "unknown PSONO_VERSION: $PSONO_VERSION" >&2
    exit 1
  fi

  echo "What insecure external port do you want to use? (Usually port 80. Redirects http to https traffic.)"
  read -p "PSONO_EXTERNAL_PORT [default: $PSONO_EXTERNAL_PORT]: " PSONO_EXTERNAL_PORT_NEW
  if [ "$PSONO_EXTERNAL_PORT_NEW" != "" ]; then
    export PSONO_EXTERNAL_PORT=$PSONO_EXTERNAL_PORT_NEW
  fi

  echo "What secure external port do you want to use? (Usually port 443. The actual port serving all the traffic with https.)"
  read -p "PSONO_EXTERNAL_PORT_SECURE [default: $PSONO_EXTERNAL_PORT_SECURE]: " PSONO_EXTERNAL_PORT_SECURE_NEW
  if [ "$PSONO_EXTERNAL_PORT_SECURE_NEW" != "" ]; then
    export PSONO_EXTERNAL_PORT_SECURE=$PSONO_EXTERNAL_PORT_SECURE_NEW
  fi

  echo "What port do you want to use for the postgres? (Usually port 5432. Leave it to 5432 to use the dockered postgres)"
  read -p "PSONO_POSTGRES_PORT [default: $PSONO_POSTGRES_PORT]: " PSONO_POSTGRES_PORT_NEW
  if [ "$PSONO_POSTGRES_PORT_NEW" != "" ]; then
    export PSONO_POSTGRES_PORT=$PSONO_POSTGRES_PORT_NEW
  fi

  echo "What is the postgres DB user you want to use (Defaults to postgres)?"
  read -p "PSONO_POSTGRES_USER [default: $PSONO_POSTGRES_USER]: " PSONO_POSTGRES_USER_NEW
  if [ "$PSONO_POSTGRES_USER_NEW" != "" ]; then
    export PSONO_POSTGRES_USER=$PSONO_POSTGRES_USER_NEW
  fi

  echo "What is the postgres DB you want to use (Defaults to postgres)?"
  read -p "PSONO_POSTGRES_DB [default: $PSONO_POSTGRES_DB]: " PSONO_POSTGRES_DB_NEW
  if [ "$PSONO_POSTGRES_DB_NEW" != "" ]; then
    export PSONO_POSTGRES_DB=$PSONO_POSTGRES_DB_NEW
  fi

  echo "What is the postgres DB address (Leave it as 'postgres' if you wannt to use the dockered postgres DB)?"
  read -p "PSONO_POSTGRES_HOST [default: $PSONO_POSTGRES_HOST]: " PSONO_POSTGRES_HOST_NEW
  if [ "$PSONO_POSTGRES_HOST_NEW" != "" ]; then
    export PSONO_POSTGRES_HOST=$PSONO_POSTGRES_HOST_NEW
  fi

  if [ "$PSONO_POSTGRES_PASSWORD" == "PSONO_IS_THE_BEST_OPEN_SOURCE_PASSWORD_MANAGER" ]; then
    export PSONO_POSTGRES_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
  fi
  echo "What password do you want to use for postgres?"
  read -p "PSONO_POSTGRES_PASSWORD [default: $PSONO_POSTGRES_PASSWORD]: " PSONO_POSTGRES_PASSWORD_NEW
  if [ "$PSONO_POSTGRES_PASSWORD_NEW" != "" ]; then
    export PSONO_POSTGRES_PASSWORD=$PSONO_POSTGRES_PASSWORD_NEW
  fi

  echo "What is the 'domain' that you will use to access your installation?"
  read -p "PSONO_WEBDOMAIN [default: $PSONO_WEBDOMAIN]: " PSONO_WEBDOMAIN_NEW
  if [ "$PSONO_WEBDOMAIN_NEW" != "" ]; then
    export PSONO_WEBDOMAIN=$PSONO_WEBDOMAIN_NEW
  fi

  echo "What is the 'domain' that your usernames should end in?"
  read -p "PSONO_USERDOMAIN [default: $PSONO_USERDOMAIN]: " PSONO_USERDOMAIN_NEW
  if [ "$PSONO_USERDOMAIN_NEW" != "" ]; then
    export PSONO_USERDOMAIN=$PSONO_USERDOMAIN_NEW
  fi

  echo "Install ACME script? (This script requires that the server is publicly accessible under $PSONO_WEBDOMAIN_NEW"
  read -p "PSONO_INSTALL_ACME [default: $PSONO_INSTALL_ACME]: " PSONO_INSTALL_ACME_NEW
  if [ "$PSONO_INSTALL_ACME_NEW" != "" ]; then
    export PSONO_INSTALL_ACME=$PSONO_INSTALL_ACME_NEW
  fi

  rm -Rf ~/.psonoenv

  cat > ~/.psonoenv <<- "EOF"
PSONO_VERSION=PSONO_VERSION_VARIABLE
PSONO_EXTERNAL_PORT=PSONO_EXTERNAL_PORT_VARIABLE
PSONO_EXTERNAL_PORT_SECURE=PSONO_EXTERNAL_PORT_SECURE_VARIABLE
PSONO_POSTGRES_PORT=PSONO_POSTGRES_PORT_VARIABLE
PSONO_POSTGRES_PASSWORD=PSONO_POSTGRES_PASSWORD_VARIABLE
PSONO_USERDOMAIN=PSONO_USERDOMAIN_VARIABLE
PSONO_WEBDOMAIN=PSONO_WEBDOMAIN_VARIABLE
PSONO_POSTGRES_USER=PSONO_POSTGRES_USER_VARIABLE
PSONO_POSTGRES_DB=PSONO_POSTGRES_DB_VARIABLE
PSONO_POSTGRES_HOST=PSONO_POSTGRES_HOST_VARIABLE
EOF

  sed -i'' -e "s,PSONO_VERSION_VARIABLE,$PSONO_VERSION,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_EXTERNAL_PORT_SECURE_VARIABLE,$PSONO_EXTERNAL_PORT_SECURE,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_EXTERNAL_PORT_VARIABLE,$PSONO_EXTERNAL_PORT,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_POSTGRES_PORT_VARIABLE,$PSONO_POSTGRES_PORT,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_POSTGRES_PASSWORD_VARIABLE,$PSONO_POSTGRES_PASSWORD,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_USERDOMAIN_VARIABLE,$PSONO_USERDOMAIN,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_WEBDOMAIN_VARIABLE,$PSONO_WEBDOMAIN,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_POSTGRES_USER_VARIABLE,$PSONO_POSTGRES_USER,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_POSTGRES_DB_VARIABLE,$PSONO_POSTGRES_DB,g" ~/.psonoenv
  sed -i'' -e "s,PSONO_POSTGRES_HOST_VARIABLE,$PSONO_POSTGRES_HOST,g" ~/.psonoenv

  cp ~/.psonoenv ~/psono/psono-quickstart/.env
}


install_acme() {
    if [ "$PSONO_INSTALL_ACME" == "1" ]; then
        echo "Install acme.sh"

        mkdir -p ~/psono/html
        curl https://get.acme.sh | sh

        ~/.acme.sh/acme.sh --issue -d $PSONO_WEBDOMAIN -w ~/psono/html

        if [ "$PSONO_VERSION" == "EE" ]; then

            /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN \
              --key-file       ~/psono/certificates/private.key  \
              --fullchain-file ~/psono/certificates/public.crt \
              --reloadcmd     "cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-ee.yml restart proxy"

            if ! crontab -l | grep "/root/.acme.sh/acme.sh --install-cert"; then
                crontab -l | {
                  cat
                  echo "0 */12 * * * /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN --key-file /root/psono/certificates/private.key --fullchain-file /root/psono/certificates/public.crt --reloadcmd \"cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-ee.yml restart proxy\" > /dev/null"
                } | crontab -
            fi

        elif [ "$PSONO_VERSION" == "CE" ]; then

            /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN \
              --key-file       ~/psono/certificates/private.key  \
              --fullchain-file ~/psono/certificates/public.crt \
              --reloadcmd     "cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-ce.yml restart proxy"

            if ! crontab -l | grep "/root/.acme.sh/acme.sh --install-cert"; then
                crontab -l | {
                  cat
                  echo "0 */12 * * * /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN --key-file /root/psono/certificates/private.key --fullchain-file /root/psono/certificates/public.crt --reloadcmd \"cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-ce.yml restart proxy\" > /dev/null"
                } | crontab -
            fi

        elif [ "$PSONO_VERSION" == "DEV" ]; then

            /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN \
              --key-file       ~/psono/certificates/private.key  \
              --fullchain-file ~/psono/certificates/public.crt \
              --reloadcmd     "cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-dev.yml restart proxy"

            if ! crontab -l | grep "/root/.acme.sh/acme.sh --install-cert"; then
                crontab -l | {
                  cat
                  echo "0 */12 * * * /root/.acme.sh/acme.sh --install-cert -d $PSONO_WEBDOMAIN --key-file /root/psono/certificates/private.key --fullchain-file /root/psono/certificates/public.crt --reloadcmd \"cd /root/psono/psono-quickstart/ && docker-compose -f docker-compose-dev.yml restart proxy\" > /dev/null"
                } | crontab -
            fi

        fi

        echo "Install acme.sh .. finsihed"
    fi
}


install_base_dependencies () {
    echo "Install curl and git"

    apt-get update &> /dev/null
    apt-get install -y curl git &> /dev/null

    echo "Install curl and git ... finished"
}


install_docker_if_not_exists () {
    echo "Install docker if it is not already installed"

    set +e
    which docker

    if [ $? -eq 0 ]
    then
        set -e
        docker --version | grep "Docker version"
        if [ $? -eq 0 ]
        then
            echo "docker exists"
        else
            curl -fsSL https://get.docker.com -o get-docker.sh
            sh get-docker.sh
            rm get-docker.sh
        fi
    else
        set -e
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh get-docker.sh
        rm get-docker.sh
    fi
    echo "Install docker if it is not already installed ... finished"
}


install_docker_compose_if_not_exists () {
    echo "Install docker compose if it is not already installed"

    set +e
    which docker-compose

    if [ $? -eq 0 ]
    then
        docker-compose --version | grep "docker-compose version"
        if [ $? -eq 0 ]
        then
            echo "docker-compose exists"
        else
            curl -L https://github.com/docker/compose/releases/download/1.20.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
            chmod +x /usr/local/bin/docker-compose
        fi
    else
        curl -L https://github.com/docker/compose/releases/download/1.20.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
    fi

    set -e

    echo "Install docker compose if it is not already installed ... finished"
}

stop_container_if_running () {
    echo "Stopping docker container"

    pushd ~/psono/psono-quickstart> /dev/null
    if [ "$PSONO_VERSION" == "EE" ]; then
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml stop
    elif [ "$PSONO_VERSION" == "CE" ]; then
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml stop
    elif [ "$PSONO_VERSION" == "DEV" ]; then
        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml stop
    fi
    popd> /dev/null
    echo "Stopping docker container ... finished"
}


create_dhparam_if_not_exists() {
    echo "Create DH params if they dont exists"
    mkdir -p ~/psono/certificates

    if [ ! -f "$HOME/psono/certificates/dhparam.pem" ]; then
        openssl dhparam -dsaparam -out ~/psono/certificates/dhparam.pem 2048
    fi
    echo "Create DH params if they dont exists ... finished"
}


create_openssl_conf () {
    echo "Create openssl config"
    mkdir -p ~/psono/certificates
    rm -Rf ~/psono/certificates/openssl.conf
    cat > ~/psono/certificates/openssl.conf <<- "EOF"
[req]
default_bits       = 2048
default_keyfile    = ~/psono/certificates/private.key
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = v3_ca

[req_distinguished_name]
countryName                 = Country Name (2 letter code)
countryName_default         = US
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = New York
localityName                = Locality Name (eg, city)
localityName_default        = Rochester
organizationName            = Organization Name (eg, company)
organizationName_default    = Psono
organizationalUnitName      = organizationalunit
organizationalUnitName_default = Development
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default          = PSONO_WEBDOMAIN
commonName_max              = 64

[req_ext]
subjectAltName = @alt_names

[v3_ca]
subjectAltName = @alt_names

[alt_names]
DNS.1   = 8.8.8.8
DNS.2   = 8.8.4.4
EOF
    sed -i'' -e "s,PSONO_WEBDOMAIN,$PSONO_WEBDOMAIN,g" ~/psono/certificates/openssl.conf
    echo "Create openssl config ... finished"
}

create_self_signed_certificate_if_not_exists () {
    echo "Create self signed certificate if it does not exist"
    mkdir -p ~/psono/certificates
    if [ ! -f "$HOME/psono/certificates/private.key" ]; then
        openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ~/psono/certificates/private.key -out ~/psono/certificates/public.crt -config ~/psono/certificates/openssl.conf
    fi
    echo "Create self signed certificate if it does not exist ... finnished"
}

create_config_json () {
    echo "Create config.json"
    mkdir -p ~/psono/config
    cat > ~/psono/config/config.json <<- "EOF"
{
  "backend_servers": [{
    "title": "Demo",
    "url": "PSONO_PROTOCOLPSONO_WEBDOMAIN/server",
    "domain": "PSONO_USERDOMAIN"
  }],
  "base_url": "PSONO_PROTOCOLPSONO_WEBDOMAIN/",
  "allow_custom_server": true
}
EOF
#    dns_ip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
#    if ifconfig | grep -q $dns_ip
#    then
#       public_url="htpp://$dns_ip:$PSONO_EXTERNAL_PORT";
#    else
#       public_url="$PSONO_PROTOCOL$PSONO_WEBDOMAIN";
#    fi
    sed -i'' -e "s,PSONO_PROTOCOL,$PSONO_PROTOCOL,g" ~/psono/config/config.json
    sed -i'' -e "s,PSONO_WEBDOMAIN,$PSONO_WEBDOMAIN,g" ~/psono/config/config.json
    sed -i'' -e "s,PSONO_USERDOMAIN,$PSONO_USERDOMAIN,g" ~/psono/config/config.json
    echo "Create config.json ... finished"
}

docker_compose_pull () {
    echo "Update docker images"

    pushd ~/psono/psono-quickstart> /dev/null
    if [ "$PSONO_VERSION" == "EE" ]; then

        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull postgres
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull psono-server
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull psono-fileserver
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull psono-client
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull psono-admin-client
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml pull psono-watchtower

    elif [ "$PSONO_VERSION" == "CE" ]; then

        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull postgres
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull psono-server
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull psono-fileserver
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull psono-client
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull psono-admin-client
        docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml pull psono-watchtower

    elif [ "$PSONO_VERSION" == "DEV" ]; then

        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml pull postgres
        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml pull psono-server
        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml pull psono-fileserver
        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml pull psono-client
        docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml pull psono-admin-client

    fi
    popd> /dev/null
    echo "Update docker images ... finished"
}

create_settings_server_yaml () {
    echo "Create settings.yml for the server"
    mkdir -p ~/psono/config
    cat > ~/psono/config/settings.yaml <<- "EOF"

SECRET_KEY: 'SOME SUPER SECRET KEY THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
ACTIVATION_LINK_SECRET: 'SOME SUPER SECRET ACTIVATION LINK SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
DB_SECRET: 'SOME SUPER SECRET DB SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
EMAIL_SECRET_SALT: '$2b$12$XUG.sKxC2jmkUvWQjg53.e'
PRIVATE_KEY: '302650c3c82f7111c2e8ceb660d32173cdc8c3d7717f1d4f982aad5234648fcb'
PUBLIC_KEY: '02da2ad857321d701d754a7e60d0a147cdbc400ff4465e1f57bc2d9fbfeddf0b'

WEB_CLIENT_URL: 'http://example.com'

ALLOWED_HOSTS: ['*']
ALLOWED_DOMAINS: ['example.com']
HOST_URL: 'http://example.com/server'

# The email used to send emails, e.g. for activation
EMAIL_FROM: 'the-mail-for-for-example-useraccount-activations@test.com'
EMAIL_HOST: 'localhost'
EMAIL_HOST_USER: ''
EMAIL_HOST_PASSWORD : ''
EMAIL_PORT: 25
EMAIL_SUBJECT_PREFIX: ''
EMAIL_USE_TLS: False
EMAIL_USE_SSL: False
EMAIL_SSL_CERTFILE:
EMAIL_SSL_KEYFILE:
EMAIL_TIMEOUT:

EMAIL_BACKEND: 'django.core.mail.backends.smtp.EmailBackend'

MANAGEMENT_ENABLED: True

# Your Postgres Database credentials
DATABASES:
    default:
        'ENGINE': 'django.db.backends.postgresql_psycopg2'
        'NAME': 'YourPostgresDatabase'
        'USER': 'YourPostgresUser'
        'PASSWORD': 'YourPostgresPassword'
        'HOST': 'YourPostgresHost'
        'PORT': 'YourPostgresPort'

# Your path to your templates folder
TEMPLATES: [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['/root/psono/templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

EOF

    sed -i'' -e "s,WEB_CLIENT_URL: 'http://example.com',WEB_CLIENT_URL: '$PSONO_PROTOCOL$PSONO_WEBDOMAIN',g" ~/psono/config/settings.yaml
    sed -i'' -e "s,HOST_URL: 'http://example.com/server',HOST_URL: '$PSONO_PROTOCOL$PSONO_WEBDOMAIN/server',g" ~/psono/config/settings.yaml
    sed -i'' -e "s,ALLOWED_DOMAINS: \['example.com'],ALLOWED_DOMAINS: ['$PSONO_USERDOMAIN'],g" ~/psono/config/settings.yaml
    sed -i'' -e "s,EMAIL_HOST: 'localhost',EMAIL_HOST: 'mail',g" ~/psono/config/settings.yaml
    sed -i'' -e "s,YourPostgresDatabase,$PSONO_POSTGRES_DB,g" ~/psono/config/settings.yaml
    sed -i'' -e "s,YourPostgresUser,$PSONO_POSTGRES_USER,g" ~/psono/config/settings.yaml
    sed -i'' -e "s,YourPostgresPassword,$PSONO_POSTGRES_PASSWORD,g" ~/psono/config/settings.yaml
    sed -i'' -e "s,YourPostgresHost,$PSONO_POSTGRES_HOST,g" ~/psono/config/settings.yaml
    sed -i'' -e "s,YourPostgresPort,$PSONO_POSTGRES_PORT,g" ~/psono/config/settings.yaml

    pushd ~/psono/psono-quickstart> /dev/null
    docker-compose -f docker-compose-ce.yml run psono-server /bin/sh -c "sleep 20 && python3 psono/manage.py migrate"
    if [ ! -f ~/.psono_server_keys ]; then
        docker-compose -f docker-compose-ce.yml run psono-server /bin/sh -c "sleep 20 && python3 psono/manage.py generateserverkeys" > ~/.psono_server_keys
    fi
    popd> /dev/null

    sed -i '/^SECRET_KEY:/d' ${HOME}/psono/config/settings.yaml
    sed -i '/^ACTIVATION_LINK_SECRET:/d' ${HOME}/psono/config/settings.yaml
    sed -i '/^DB_SECRET:/d' ${HOME}/psono/config/settings.yaml
    sed -i '/^EMAIL_SECRET_SALT:/d' ${HOME}/psono/config/settings.yaml
    sed -i '/^PRIVATE_KEY:/d' ${HOME}/psono/config/settings.yaml
    sed -i '/^PUBLIC_KEY:/d' ${HOME}/psono/config/settings.yaml

    echo -e "$(cat ~/.psono_server_keys)\n$(cat ${HOME}/psono/config/settings.yaml)" > ${HOME}/psono/config/settings.yaml
    echo "Create settings.yml for the server ... finished"
}

create_settings_fileserver_yaml () {
    echo "Create settings.yml for the fileserver"
    mkdir -p ~/psono/config
    cat > ~/psono/config/settings-fileserver.yaml <<- "EOF"

SERVER_URL: 'https://example.com/server'
ALLOWED_HOSTS: ['*']
HOST_URL: 'https://example.com/fileserver01'

EOF

    sed -i'' -e "s,SERVER_URL: 'https://example.com/server',SERVER_URL: 'http://psono-server',g" ~/psono/config/settings-fileserver.yaml
    sed -i'' -e "s,HOST_URL: 'https://example.com/fileserver01',HOST_URL: '$PSONO_PROTOCOL$PSONO_WEBDOMAIN/fileserver',g" ~/psono/config/settings-fileserver.yaml

    pushd ~/psono/psono-quickstart> /dev/null
    docker-compose -f docker-compose-ce.yml run psono-server /bin/sh -c "sleep 20 && python3 psono/manage.py fsclustercreate 'Test Cluster' --fix-cluster-id=d5d8fea5-3c9c-4a3c-97db-8d50dd2f473c && python3 psono/manage.py fsshardcreate 'Test Shard' 'Test Shard Description' --fix-shard-id=5575b1a3-0d99-41bb-aa76-8277236ba90b && python3 psono/manage.py fsshardlink d5d8fea5-3c9c-4a3c-97db-8d50dd2f473c 5575b1a3-0d99-41bb-aa76-8277236ba90b  --fix-link-id=324ebf85-09fe-4172-87c6-09fdf7a7c108"
    docker-compose -f docker-compose-ce.yml run psono-server /bin/sh -c "sleep 20 && python3 psono/manage.py fsclustershowconfig d5d8fea5-3c9c-4a3c-97db-8d50dd2f473c" > ~/.psono_fileserver_server_keys
    popd> /dev/null

    sed -i '/^SERVER_URL:/d' ~/.psono_fileserver_server_keys

    echo -e "$(cat ~/.psono_fileserver_server_keys)\n$(cat ${HOME}/psono/config/settings-fileserver.yaml)" > ${HOME}/psono/config/settings-fileserver.yaml

    rm  ~/.psono_fileserver_server_keys
    echo "Create settings.yml for the fileserver ... finished"
}

build_psono_proxy () {
    echo "Build psono proxy"
    docker build -t psono-proxy ~/psono/psono-quickstart
    echo "Build psono proxy ... finished"
}

start_stack () {
    echo "Start the stack"

    pushd ~/psono/psono-quickstart> /dev/null

    if [ "$PSONO_VERSION" == "EE" ]; then

        docker-compose -f docker-compose-ee.yml up -d

    elif [ "$PSONO_VERSION" == "CE" ]; then

        docker-compose -f docker-compose-ce.yml up -d

    elif [ "$PSONO_VERSION" == "DEV" ]; then

        docker-compose -f docker-compose-dev.yml up -d

    fi

    popd> /dev/null

    echo "Start the stack ... finished"
}

install_alias () {
    echo "Install alias"

    if [ ! -f ~/.bash_aliases ]; then
        touch ~/.bash_aliases
    fi

    sed -i '/^alias psonoctl=/d' ~/.bash_aliases

    if [ "$PSONO_VERSION" == "EE" ]; then

        alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f ~/psono/psono-quickstart/docker-compose-ee.yml'
        echo -e "alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f docker-compose-ee.yml'\n$(cat ~/.bash_aliases)" > ~/.bash_aliases

    elif [ "$PSONO_VERSION" == "CE" ]; then

        alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f ~/psono/psono-quickstart/docker-compose-ce.yml'
        echo -e "alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f docker-compose-ce.yml'\n$(cat ~/.bash_aliases)" > ~/.bash_aliases

    elif [ "$PSONO_VERSION" == "DEV" ]; then

        alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f ~/psono/psono-quickstart/docker-compose-dev.yml'
        echo -e "alias psonoctl='cd ~/psono/psono-quickstart && docker-compose -f docker-compose-dev.yml'\n$(cat ~/.bash_aliases)" > ~/.bash_aliases

    fi

    echo "Install alias ... finished"
}

detect_os () {
    echo "Start detect OS"
    if [ `which lsb_release 2>/dev/null` ]; then
        DIST=`lsb_release -c | cut -f2`
        OS=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`
    else
        echo "Unknown OS" >&2
        exit 1
    fi
    if [ "$OS" != "ubuntu" ]; then
        echo "Unsupported OS" >&2
        exit 1
    fi

    echo "Detected $OS: $DIST"

    echo "Start detect OS ... finnished"
}

install_quickstart() {
    mkdir -p ~/psono
    rm -Rf ~/psono/psono-quickstart
    git clone https://gitlab.com/psono/psono-quickstart.git ~/psono/psono-quickstart
}

main() {

    detect_os

    install_base_dependencies

    install_quickstart

    install_docker_if_not_exists

    install_docker_compose_if_not_exists

    ask_parameters

    stop_container_if_running

    mkdir -p ~/psono/html
    mkdir -p ~/psono/data/postgresql
    mkdir -p ~/psono/data/mail
    mkdir -p ~/psono/data/shard

    if [ "$PSONO_VERSION" == "DEV" ]; then
        echo "Checkout psono-server git repository"
        if [ ! -d "$HOME/psono/psono-server" ]; then
            git clone https://gitlab.com/psono/psono-server.git ~/psono/psono-server
        fi
        echo "Checkout psono-server git repository ... finished"
        echo "Checkout psono-client git repository"
        if [ ! -d "$HOME/psono/psono-client" ]; then
            git clone https://gitlab.com/psono/psono-client.git ~/psono/psono-client
        fi
        echo "Checkout psono-client git repository ... finished"
        echo "Checkout psono-fileserver git repository"
        if [ ! -d "$HOME/psono/psono-fileserver" ]; then
            git clone https://gitlab.com/psono/psono-fileserver.git ~/psono/psono-fileserver
        fi
        echo "Checkout psono-fileserver git repository ... finished"
    fi


    create_dhparam_if_not_exists

    create_openssl_conf

    create_self_signed_certificate_if_not_exists

    create_config_json

    docker_compose_pull

    create_settings_server_yaml

    create_settings_fileserver_yaml

    build_psono_proxy

    start_stack

    install_acme

    install_alias

    echo ""
    echo "========================="
    echo "CLIENT URL : https://$PSONO_WEBDOMAIN"
    echo "ADMIN URL : https://$PSONO_WEBDOMAIN/portal/"
    echo ""
    echo "USER1: demo1@$PSONO_USERDOMAIN"
    echo "PASSWORD: demo1"
    echo ""
    echo "USER2: demo2@$PSONO_USERDOMAIN"
    echo "PASS: demo2"
    echo ""
    echo "ADMIN: admin@$PSONO_USERDOMAIN"
    echo "PASS: admin"
    echo "========================="
    echo ""
}

main
