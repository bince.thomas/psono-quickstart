FROM nginx:alpine
MAINTAINER Sascha Pfeiffer <sascha.pfeiffer@psono.com>

COPY nginx.conf /etc/nginx/nginx.conf